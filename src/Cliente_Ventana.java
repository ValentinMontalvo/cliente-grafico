
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import test.Cliente; 
import test.Servidor;


public class Cliente_Ventana extends javax.swing.JFrame implements Runnable {
    
    

    DefaultListModel listaUsuarios = new DefaultListModel();
    BufferedReader in;
    PrintWriter out;
    Socket cnx;

    public Cliente_Ventana() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        listaUsuarios = new DefaultListModel();
        JlistUsuarios.setModel(listaUsuarios);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        label_mensaje = new javax.swing.JLabel();
        label_usuario = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        chat = new javax.swing.JTextArea();
        mensaje = new javax.swing.JTextField();
        enviar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JlistUsuarios = new javax.swing.JList<>();
        salida = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jPanel1.setBackground(new java.awt.Color(255, 102, 102));

        label_mensaje.setFont(new java.awt.Font("Cambria Math", 1, 18)); // NOI18N
        label_mensaje.setForeground(new java.awt.Color(255, 255, 255));
        label_mensaje.setText("Mensajes");

        label_usuario.setFont(new java.awt.Font("Cambria Math", 1, 18)); // NOI18N
        label_usuario.setForeground(new java.awt.Color(255, 255, 255));
        label_usuario.setText("Usuarios");

        chat.setEditable(false);
        chat.setBackground(new java.awt.Color(0, 204, 204));
        chat.setColumns(20);
        chat.setFont(new java.awt.Font("Cambria Math", 1, 14)); // NOI18N
        chat.setForeground(new java.awt.Color(255, 255, 255));
        chat.setRows(5);
        jScrollPane3.setViewportView(chat);

        mensaje.setBackground(new java.awt.Color(0, 204, 204));
        mensaje.setFont(new java.awt.Font("Cambria Math", 1, 14)); // NOI18N
        mensaje.setForeground(new java.awt.Color(255, 255, 255));
        mensaje.setText("Escriba el mensaje");
        mensaje.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mensajeMouseClicked(evt);
            }
        });
        mensaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mensajeActionPerformed(evt);
            }
        });

        enviar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/final_envio.png"))); // NOI18N
        enviar.setOpaque(false);
        enviar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                enviarMouseClicked(evt);
            }
        });
        enviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enviarActionPerformed(evt);
            }
        });

        JlistUsuarios.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(JlistUsuarios);

        salida.setText("Salir");
        salida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salidaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(salida)
                    .addComponent(enviar, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(137, 137, 137)
                                .addComponent(label_mensaje))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                            .addComponent(mensaje))
                        .addGap(46, 46, 46)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label_usuario)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_usuario)
                    .addComponent(label_mensaje))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(enviar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mensaje))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(salida)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void enviarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_enviarMouseClicked

    }//GEN-LAST:event_enviarMouseClicked

    private void enviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enviarActionPerformed
        String mensaje = this.mensaje.getText();
        this.send(mensaje);
        this.mensaje.setText("");
        chat.setText(chat.getText() + " " + mensaje + "\n");
    }//GEN-LAST:event_enviarActionPerformed

    private void mensajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mensajeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mensajeActionPerformed

    private void mensajeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mensajeMouseClicked
        mensaje.setText("");
    }//GEN-LAST:event_mensajeMouseClicked

    private void salidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salidaActionPerformed
        System.exit(0);
    }//GEN-LAST:event_salidaActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cliente_Ventana().run();
            }
        });

       
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> JlistUsuarios;
    private javax.swing.JTextArea chat;
    private javax.swing.JButton enviar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel label_mensaje;
    private javax.swing.JLabel label_usuario;
    private javax.swing.JTextField mensaje;
    private javax.swing.JButton salida;
    // End of variables declaration//GEN-END:variables

    void start() {
        ConexionCliente hilo;
        try {
            this.cnx = new Socket("192.168.1.66", 4444);
            in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            out = new PrintWriter(cnx.getOutputStream(), true);

            hilo = new ConexionCliente(in);
            hilo.start();

        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void send(String mensaje) {
        out.println(mensaje);
    }

    @Override
    public void run() {
        start();
        this.setVisible(true);
    }

    
    
    
    
    class ConexionCliente extends Thread {
        
       
        
         

        public boolean ejecutar = true;
        BufferedReader in;

        public ConexionCliente(BufferedReader in) {
            this.in = in;
        }
        
        
        
        @Override
        public void run() {
            String respuesta = "";
            boolean login = false;
            String user = null;
            while (ejecutar) {
                try {
                    respuesta = in.readLine();
                    if (respuesta != null) {

                        while (!login) {
                            if (respuesta.equals("Bienvenido, proporcione su usuario")) {
                                user = JOptionPane.showInputDialog("Ingrese su nombre");
                                out.println(user);

                                                         
                                
                                if (null != user) {
                                    switch (user) {
                                        case "hugo":
                                            listaUsuarios.addElement("hugo");
                                            break;
                                        case "luis":
                                            listaUsuarios.addElement("luis");
                                            break;
                                        case "paco":
                                            listaUsuarios.addElement("paco");
                                            break;
                                        case "donald":
                                            listaUsuarios.addElement("donald");
                                            break;
                                        case "jose":
                                            listaUsuarios.addElement("jose");
                                            break;
                                        default:
                                            break;
                                    }
                                }

                            } else if (respuesta.equals("Escriba el password")) {
                                String pass = JOptionPane.showInputDialog("Ingrese su contraseña");
                                out.println(pass);
                            } else {
                                login = !login;
                            }
                            respuesta = in.readLine();
                        }
                        chat.setText(chat.getText() + respuesta + "\n");

                        if (respuesta.contains(" Se ha unido a la conversacion.")) {
                            listaUsuarios.addElement(respuesta.substring(0, 4));
                        } else if (respuesta.contains("ha salido del chat.")) {
                            listaUsuarios.removeElement(respuesta.substring(0, 4));
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        
        
        
        
    }

}
